module.exports = () => {
    const argList = process.argv.slice(2);

    const args = {};
    let key = '';
    let value = true;
    for (const arg of argList) {
        if (arg.startsWith('-')) {
            const filteredArg = arg.replace(/\-/g, '');
            if (key !== '') {
                args[key] = value;
            }
            key = filteredArg;
            value = true;
        } else {
            if (key !== '') {
                value = arg;
            }
        }
    }

    if (key) {
        args[key] = value;
    }

    return args;
}