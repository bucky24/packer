const fs = require('fs');
const path = require('path');

const builder = require('./builder');
const getArgs = require('./args');

let { entry, template, babel } = getArgs();

const buildDir = path.resolve(__dirname, "build");
if (fs.existsSync(buildDir)) {
    fs.rmSync(buildDir, { recursive: true });
}
fs.mkdirSync(buildDir);
(async () => {
    const { files } = await builder(entry, template, buildDir, babel);
    for (const file in files) {
        const content = files[file];

        fs.mkdirSync(path.dirname(file), { recursive: true })

        fs.writeFileSync(file, content);
    }
})();