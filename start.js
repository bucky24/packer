const fs = require('fs');
const path = require('path');
const urlParse = require('url');
const http = require('http');

const builder = require('./builder');
const getArgs = require('./args');

let { entry, template, babel } = getArgs();

const buildDir = path.resolve(__dirname, "build");

(async () => {
    let builderData = {};
    const builderResult = await builder(entry, template, buildDir, babel);
    builderData = {...builderResult};

    const listeners = [];
    let listenerProcessing = null;

    function registerListeners(files) {
        const unregister = Object.keys(listeners).filter(x => !files.includes(x));
        const register = files.filter(x => !Object.keys(listeners).includes(x));
        register.forEach((file) => {
            listeners[file] = fs.watch(file, {}, async (event, filename) => {
                if (listenerProcessing !== null) {
                    return;
                }
                listenerProcessing = file;
                if (event === 'change') {
                    console.log('Detected change in ', file);
                    // time to recompile!!!
                    // inefficient right now because it regnereates the entire file tree from this point
                    const newData = await builder(file, template, buildDir, babel);
                    const newFiles = newData.allSourceFiles.filter(x => !builderData.allSourceFiles.includes(x));
                    const updateFiles = [
                        ...newFiles,
                        file,
                    ];

                    updateFiles.forEach((updateFile) => {
                        const buildFile = newData.sourceDestMap[updateFile];
                        builderData.meta[buildFile] = newData.meta[buildFile];
                        builderData.files[buildFile] = newData.files[buildFile];
                    });
                }
                listenerProcessing = null;
            });
        });

        unregister.forEach((file) => {
            listeners[file].close();
            delete listeners[file];
        })
    }

    registerListeners(builderData.allSourceFiles);

    const port = 8080;

    const server = http.createServer((req, res) => {
        const resHeaders = {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS, POST, GET, PATCH, DELETE, PUT',
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Max-Age': 2592000, // 30 days
        };

        //console.log(req.method);

        if (req.method === 'OPTIONS') {
            res.writeHead(204, resHeaders);
            res.end();
            return;
        }

        let requestData = '';
        req.on('data', chunk => {
            requestData += chunk.toString('utf8');
        })
        req.on('end', () => {
            const { url } = req;
            const urlObj = urlParse.parse(url, true)
            const { pathname } = urlObj
            //console.log(pathname);

            // basically either we're serving css, js, or html. Any other request gets index.html

            let isIndex = true;
            let mimeType = 'text/html';
            const checkPaths = {
                '.js': 'text/javascript',
                '.css': 'text/css',
                '.html': 'text/html',
            };
            for (const checkPath in checkPaths) {
                if (pathname.endsWith(checkPath)) {
                    isIndex = false;
                    mimeType = checkPaths[checkPath];
                    break;
                }
            }

            let fileName = path.resolve(pathname);
            if (isIndex || pathname === '\index.html') {
                fileName = builderData.paths.index;
            }

            const buildFileName = path.join(buildDir, pathname);
            if (builderData.files[buildFileName]) {
                fileName = buildFileName;
            }

            // serve whatever file we came up with

            //console.log(builderData);

            if (builderData.files[fileName]) {
                const data = builderData.meta[fileName];
                console.log("Serving", pathname,"from build cache with type", data.type);
                res.setHeader('Content-type', data.type);
                res.writeHead(200);
                res.end(builderData.files[fileName]);
            } else {
                const realPath = path.join(__dirname, pathname);
                console.log(__dirname, pathname, realPath);
                if (!fs.existsSync(realPath)) {
                    console.log("Cannot find path",pathname,"(",realPath,")");
                    res.writeHead(404);
                    res.end();
                } else {
                    console.log("Serving", pathname,"from file system (", realPath, ") with type", mimeType);
                    const content = fs.readFileSync(realPath, 'utf8');
                    res.setHeader('Content-type',mimeType);
                    res.writeHead(200);
                    res.end(content);
                }
            }
        });
    });

    server.listen(port, () => {
        console.log('Packing started on port ' + port);
    });
})();