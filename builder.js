const path = require('path');
const fs = require('fs');
const { exec } = require('child_process');

function callExec(command) {
    return new Promise((resolve, reject) => {
        exec(command, (error, stdout, stderr) => {
            resolve({
                error,
                stdout,
                stderr,
            });
        });
    });
}

module.exports = async function(entry, template, buildDir, babelCommand) {
    entry = path.resolve(entry);
    template = path.resolve(template);

    const files = {};
    const meta = {};
    const paths = {};
    const allSourceFiles = [];
    const sourceDestMap = {};

    const cwd = process.cwd();

    const cacheFile = path.join(buildDir, "cache_file.js");

    const processFile = async (file, processedFiles = []) => {
        const dir = path.dirname(file);

        if (processedFiles.includes(file)) {
            return;
        }

        if (!fs.existsSync(file)) {
            file += '.js';
            if (!fs.existsSync(file)) {
                return;
            }
        }

        if (!allSourceFiles.includes(file)) {
            allSourceFiles.push(file);
        }

        const relativeFile = path.relative(cwd, file);
        const fileBuildPath = path.join(buildDir, relativeFile);
        sourceDestMap[file] = fileBuildPath;
        const fileBuildDir = path.dirname(fileBuildPath);

        //console.log(file, fileBuildPath);

        let contents = fs.readFileSync(file, 'utf8');

        const matches = [...contents.matchAll(/import .{0,}( from ){0,1}['"](.+)['"];/g)];

        for (const match of matches) {
            const matchPath = match[2];
            const actualPath = path.resolve(dir, matchPath);

            const buildPath = await processFile(actualPath);
            if (buildPath) {
                //console.log('path', buildPath, buildDir);
                let relativePath = path.relative(fileBuildDir, buildPath);
                if (!relativePath.startsWith('.')) {
                    // it's probably on same level
                    relativePath = "./" + relativePath;
                }
                //console.log('turned into', relativePath);
                contents = contents.replace(matchPath, relativePath);
            }
        }

        let mimeType = 'text/html';
        const checkPaths = {
            '.js': 'text/javascript',
            '.css': 'text/css',
            '.html': 'text/html',
        };
        for (const checkPath in checkPaths) {
            if (fileBuildPath.endsWith(checkPath)) {
                isIndex = false;
                mimeType = checkPaths[checkPath];
                break;
            }
        }

        // now run it through babel
        if (babelCommand) {
            fs.writeFileSync(cacheFile, contents);
            const command = 'npm run --silent ' + babelCommand + " -f " + cacheFile;
            const result = await callExec(command);
            contents = result.stdout;
        }
        //console.log(result);

        files[fileBuildPath] = contents;
        meta[fileBuildPath] = { type: mimeType };
        return fileBuildPath;
    }

    const entryBuildPath = await processFile(path.resolve(cwd, entry));

    const templateCode = fs.readFileSync(template, 'utf8');

    //console.log(templateCode);

    const headPos = templateCode.indexOf('<head>');
    const headEndPos = templateCode.indexOf('</head>');

    const beforeHead = templateCode.substring(0, headPos);
    const inHead = templateCode.substring(headPos+6, headEndPos);
    const afterHead = templateCode.substring(headEndPos+7);

    const scriptCode = "<script src=\"./" + path.relative(buildDir, entryBuildPath) + "\" type=\"module\"></script>";

    const finalCode = beforeHead + "<head>" + inHead + scriptCode + "</head>" + afterHead;

    //console.log(finalCode);

    let templateName = path.basename(template);
    templateName = templateName.replace('.tmpl', '');

    const templatePath = path.resolve(buildDir, templateName);

    files[templatePath] = finalCode;

    paths.index = templatePath;

    meta[templatePath] = { type: 'text/html' };

    if (fs.existsSync(cacheFile)) {
        fs.rmSync(cacheFile);
    }

    return {
        files,
        paths,
        meta,
        allSourceFiles,
        sourceDestMap,
    };
}